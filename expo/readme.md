# Expo

Expo just a collection of tools to create, build and manager mobile app development in React native

You can create an account at https://expo.dev/ 

# Start



In your CLI, run

``npx create-expo-app YOUR-APP-NAM``

This will create a expo manageable react app with the name YOUR-APP-NAME. You can give any name

## Upgrading - Simple Project 


These are  steps to import your old project into the new one created above. 

1. Upgrade eas

  ``npm i -g eas-cli``

2. Copy the 'dependencies' section of package.json from old app into newer one. Then run

    ``npx expo install --fix``

3. IF it shows  a warning "Some dependancies are incombatible...". Then you have to install correct verison

    Example ; Some dependencies are incompatible with the installed expo version:

    ``expo-application@4.0.2 - expected version: ~5.1.1``

    ``expo-barcode-scanner@11.2.1 - expected version: ~12.3.2``

    Install individual packages by running ``npx expo install expo-application@~5.1.1 expo-barcode-scanner@~12.3.2``

4. Then run

    ``npx expo-doctor``

    This may list incompatible , conflicting and prebuild issues. Example of pre build support  package compatibility,

    Expected package expo-modules-autolinking@~1.1.0
    Found invalid:
     expo-modules-autolinking@0.5.5

    run

     ``npm why expo-modules-autolinking``

    This will show the dependancy package that you have to install or upgrade.

    Remove that package from package.json, then install it again using

     ``npm install expo-modules-autolinking``.

    Note, you have to replace "expo-modules-autolinking" with the actual package having issues in your app

 5. Upgrade Expo Go app in your device
 
 6. Upgrade xcode if you use 'ios'

    For further information refer https://blog.expo.dev/expo-sdk-48-ccb8302e231 
 (Note, this is specific to expo 48, but you have to find latest version from your package.json, the )





